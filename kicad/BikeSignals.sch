EESchema Schematic File Version 4
EELAYER 28 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title "Bike Turn Signals"
Date ""
Rev "1"
Comp "demilleTech LLC"
Comment1 "Credit to SparkFun and their Fio v3 for the base microcontroller setup."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SparkFun-PowerSymbols:GND #GND02
U 1 1 5BCD9A66
P 20050 7650
F 0 "#GND02" H 20050 7650 45  0001 L BNN
F 1 "GND" V 20050 7621 45  0000 R CNN
F 2 "" H 20050 7650 50  0001 C CNN
F 3 "" H 20050 7650 50  0001 C CNN
	1    20050 7650
	-1   0    0    1   
$EndComp
$Comp
L SparkFun-PowerSymbols:GND #GND01
U 1 1 5BCE5625
P 10100 18050
F 0 "#GND01" H 10150 18000 45  0001 L BNN
F 1 "GND" V 10100 17922 45  0000 R CNN
F 2 "" H 10100 17950 60  0001 C CNN
F 3 "" H 10100 17950 60  0001 C CNN
	1    10100 18050
	0    1    1    0   
$EndComp
$Sheet
S 1000 1000 500  500 
U 5BD8CBB3
F0 "Main MCU Board" 50
F1 "MainMCU_Board.sch" 50
$EndSheet
$EndSCHEMATC
